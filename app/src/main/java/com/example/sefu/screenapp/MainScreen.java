package com.example.scudo.screenActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

    }

    public void callScreenOne(View view) {
        Intent screenOne = new Intent(getApplicationContext(), ScreenOne.class);
        startActivity(screenOne);
    }

    public void callScreenTwo(View view) {
        Intent screenTwo = new Intent(getApplicationContext(), ScreenTwo.class);
        startActivity(screenTwo);
    }

    public void callScreenThree(View view) {
        Intent screenThree = new Intent(getApplicationContext(), ScreenThree.class);
        startActivity(screenThree);
    }
}
